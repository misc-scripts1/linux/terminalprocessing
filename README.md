# TerminalProcessing
This is a collection of linux bash scripts that will send a constant stream of text to your terminal.

## Instructions
1. Download the latest version (https://bitbucket.org/matthewlinton/terminalprocessing/downloads)
1. Unpack the zip file.
1. Navigate to the TerminalProcessing directory.
1. Run "processing.sh"

For help on the options available run "processing.sh -h"

## About
The "processing.sh" script ties a number of other scripts together and sends a
constant stream of data to the terminal.

## License
This code is not covered under any license. You are welcome to modify and share this code as you please.

## Liability
Use TerminalProcessing at your own risk!

TerminalProcessing, to the best of my knowledge, does not contain any harmful or malicious code. I assume no liability for any issues that may occur from the use of this software. Please take the time to understand how this code will interact with your system before using it.